/**
 * Licensed to the Scalable Computing Software (SCS) Lab under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The SCS licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main( int argc, char *argv[] )
{
    int rc;
    int rank;
    MPI_File fh, fd;
    char * buf;
    MPI_Status status;
    char *source = argv[1];
    char * distination = argv[2];
    MPI_Offset offset = atoll(argv[3]);
    int count = atoi(argv[4]);
    int redundant = 2048;
    MPI_Offset size = 0; 
    MPI_Init( &argc, &argv );
    MPI_Comm_rank( MPI_COMM_WORLD, &rank );

    rc = MPI_File_open( MPI_COMM_WORLD, source, MPI_MODE_RDONLY, MPI_INFO_NULL, &fh );
    if (rc) {
        printf( "Unable to open file %s\n", source );fflush(stdout);
    }

    int sync = 8;
    if(offset == 0) sync = 0; 

    MPI_File_get_size(fh, &size);
    if(size <= count+offset+redundant){ //TODO 
        count += sync; //it is the last block/split, so adjust the count again. recall we will read 8 more byte.  
    }else{
	count += sync + redundant; 
    }

    char *PREF = "PREF";
    char *pref_file = malloc(strlen(distination)+strlen(PREF)+1);//+1 for the zero-terminator
    strcpy(pref_file, distination);
    strcat(pref_file, PREF);
    MPI_File pf;
    rc = MPI_File_open( MPI_COMM_WORLD, pref_file, MPI_MODE_RDWR | MPI_MODE_CREATE, MPI_INFO_NULL, &pf );
    MPI_File_close( &pf);

    buf = (char *) malloc( count * sizeof(char));
    MPI_File_read_at(fh, offset, buf, count, MPI_CHAR, &status );
    MPI_File_close( &fh );

    rc = MPI_File_open( MPI_COMM_WORLD, distination, MPI_MODE_RDWR | MPI_MODE_CREATE, MPI_INFO_NULL, &fd );
    MPI_File_write(fd, buf, count, MPI_CHAR, &status);
    MPI_File_close( &fd);
    free(buf);

    char *SYNC = "SYNC";
    char *flag_file = malloc(strlen(distination)+strlen(SYNC)+1);//+1 for the zero-terminator
    strcpy(flag_file, distination);
    strcat(flag_file, SYNC);

    MPI_File fg;
    printf("sync file name %s\n", flag_file);


    rc = MPI_File_open( MPI_COMM_WORLD, flag_file, MPI_MODE_RDWR | MPI_MODE_CREATE, MPI_INFO_NULL, &fg );
    MPI_File_close( &fg);

    MPI_Finalize();
    return 0;
}
