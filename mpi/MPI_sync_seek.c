/**
 * Licensed to the Scalable Computing Software (SCS) Lab under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The SCS licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/time.h>

/**
 * Modified by: Bo Feng
 * Date: 06/23/2015
 */

#define BLK_SIZE 67108864 // 64MB
typedef struct {
  char source[128];
  char dest[128];
  MPI_File src_fh;
  MPI_File dst_fh;
  MPI_Offset offset;
  long length;
} FileChunk;

FileChunk * openFileChunk(char * source, char * dest, long offset, long length)
{
  int rc;
  FileChunk * chunk = (FileChunk *)malloc(sizeof(FileChunk));
  strcpy(chunk->source, source);
  strcpy(chunk->dest, dest);
  chunk->offset = offset;
  chunk->length = length;

  rc = MPI_File_open(MPI_COMM_WORLD, chunk->source, MPI_MODE_RDONLY, MPI_INFO_NULL, &chunk->src_fh);
  if (rc)
  {
    printf("Unable to open file %s\n", chunk->source);
    free(chunk);
    return NULL;
  }
  rc = MPI_File_open(MPI_COMM_WORLD, chunk->dest, MPI_MODE_RDWR | MPI_MODE_CREATE, MPI_INFO_NULL, &chunk->dst_fh);
  if (rc)
  {
    printf("Unable to open file %s\n", chunk->dest);
    free(chunk);
    return NULL;
  }
  return chunk;
}

int closeFileChunk(FileChunk * chunk)
{
  int rc;
  rc = MPI_File_close(&(chunk->src_fh));
  rc = MPI_File_close(&(chunk->dst_fh));

  //todo: error check

  return rc;
}

void printFileChunk(FileChunk * chunk)
{
  printf("Source file path:\t%s\n", chunk->source);
  printf("Destination file path:\t%s\n", chunk->dest);
}

void countSyncMarkers(
    FileChunk * chunk,
    int range,
    long * numberOfMarkers)
{
  MPI_Offset filesize;
  long numberOfBlocks;

  MPI_File_get_size(chunk->src_fh, &filesize);

  numberOfBlocks = filesize % BLK_SIZE == 0?filesize/BLK_SIZE:filesize/BLK_SIZE+1;

  printf("file size: %ld\n", filesize);
  printf("# of blocks: %ld\n", numberOfBlocks);

  *numberOfMarkers = (numberOfBlocks - 1) * 2;
}

void lookupSyncMarkers(
    FileChunk * chunk,
    int range,
    long numberOfMarkers,
    MPI_Offset * syncMarkers)
{
  int i = 0, j = 0;
  MPI_Offset offset, length;
  char * buffer;
  MPI_Status status;
  for (i = 1; i < numberOfMarkers / 2 + 1; i++)
  {
    offset = (MPI_Offset)(BLK_SIZE) * i - 1 - range;
    length = 2 * range;
    printf("[%d]:%ld ~ %ld\n", i, offset, length);
    buffer = (char *) malloc(length * sizeof(char));
    MPI_File_read_at(chunk->src_fh, offset, buffer, length, MPI_CHAR, &status);

    for (j = length/2 - 1; j >= 0; j--)
    {
      if (isspace(buffer[j]))
      {
        syncMarkers[(i-1)*2] = offset + j;
        break;
      }
    }
    for (j = length/2; j < length; j++)
    {
      if (isspace(buffer[j]))
      {
        syncMarkers[(i-1)*2+1] = offset + j;
        break;
      }
    }
    free(buffer);
  }
}

/**
 * assume: numberOfInputMarkers is two times of number of output markers
 */
void reduceSyncMarkers(
    MPI_Offset * inputMarkers,
    long numberOfInputMarkers,
    MPI_Offset * outputMarkers)
{
  long i;
  MPI_Offset boundary;
  for (i = 0; i < numberOfInputMarkers; i+=2)
  {
    boundary = (MPI_Offset) BLK_SIZE * (i/2 + 1) - 1;
    printf("[%ld:%ld]%ld <> %ld\n", i, boundary, inputMarkers[i], inputMarkers[i+1]);
    if (boundary - inputMarkers[i] >= inputMarkers[i+1] - boundary)
    {
      outputMarkers[i/2] = inputMarkers[i+1];
    }
    else
    {
      outputMarkers[i/2] = inputMarkers[i];
    }
  }
}

void listDirectories(const char * folder)
{
}

int main( int argc, char *argv[] )
{
  int rc;
  int rank;
  MPI_File fh, fd;
  char * buf;
  MPI_Status status;
  char *source = argv[1];
  char * distination = argv[2];
  MPI_Offset offset = atoll(argv[3]);
  int count = atoi(argv[4]);
  int redundent = 2048;//assume no words will execess this len
  MPI_Offset size = 0;
  MPI_Init( &argc, &argv );
  MPI_Comm_rank( MPI_COMM_WORLD, &rank );

  FileChunk * chunk = openFileChunk(argv[1], argv[2], atoll(argv[3]), atoll(argv[4]));

  printFileChunk(chunk);

  int i;
  long numberOfMarkers;
  long numberOfReducedSyncMarkers;
  MPI_Offset * syncMarkers;
  MPI_Offset * reducedSyncMarkers;

  countSyncMarkers(chunk, 1024, &numberOfMarkers);
  printf("# of Markers: %d\n", numberOfMarkers);

  numberOfReducedSyncMarkers = numberOfMarkers / 2;

  syncMarkers = (MPI_Offset*)calloc(sizeof(MPI_Offset), numberOfMarkers);
  reducedSyncMarkers = (MPI_Offset*)calloc(sizeof(MPI_Offset), numberOfReducedSyncMarkers);

  lookupSyncMarkers(chunk, 1024, numberOfMarkers, syncMarkers);
  reduceSyncMarkers(syncMarkers, numberOfMarkers, reducedSyncMarkers);

  for (i = 0; i < numberOfMarkers; i++)
  {
    printf("SYNC Marker[%d]: %ld\n", i, syncMarkers[i]);
  }
  for (i = 0; i < numberOfReducedSyncMarkers; i++)
  {
    printf("Final SYNC Marker[%d]: %ld\n", i, reducedSyncMarkers[i]);
  }

  free(syncMarkers);
  free(reducedSyncMarkers);

  closeFileChunk(chunk);

  MPI_Finalize();

  return 0; //disable following code
}
