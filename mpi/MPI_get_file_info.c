/**
 * Licensed to the Scalable Computing Software (SCS) Lab under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The SCS licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/time.h>


MPI_Offset main( int argc, char *argv[] )
{
    int rc;
    int rank;
    MPI_File fh, fd;
    MPI_Status status;
    char *source = argv[1];
    MPI_Offset size = 0; 
    MPI_Init( &argc, &argv );
    MPI_Comm_rank( MPI_COMM_WORLD, &rank );
    rc = MPI_File_open( MPI_COMM_WORLD, source, MPI_MODE_RDONLY, MPI_INFO_NULL, &fh );
    if (rc) {
        printf( "Unable to open file %s\n", source );fflush(stdout);
    }
    MPI_File_get_size(fh, &size);
    MPI_File_close( &fh);
    printf("%s %lld", source, size);
    MPI_Finalize();

    return size;
}
