/**
 * Licensed to the Scalable Computing Software (SCS) Lab under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The SCS licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>

int main(int argc, char ** argv)
{
  if(argc != 2)
  {
    puts("Usage: list_dir <path>");
    return 1;
  }

  DIR *dp;
  struct dirent *ep;
  int input_file;
  struct stat input_file_stat;
  int file;
  struct stat file_stat;
  char file_path[128];

  if((input_file = open(argv[1], O_RDONLY)) != -1)
  {
    if(fstat(input_file, &input_file_stat) == 0)
    {
      if(S_ISREG(input_file_stat.st_mode))
      {
        printf("%s#%lld\n", argv[1] + LEN_OF_PATH, (long long)input_file_stat.st_size);
      }
      else if(S_ISDIR(input_file_stat.st_mode))
      {
        dp = opendir(argv[1]);

        if(dp != NULL)
        {
          while(ep = readdir (dp))
          {
            strcpy(file_path, argv[1]);
            strcat(file_path, "/");
            strcat(file_path, ep->d_name);
            if((file = open(file_path, O_RDONLY)) < -1)
              continue;
            if(fstat(file, &file_stat) < 0)
              continue;

            if(S_ISREG(file_stat.st_mode)){
              printf("%s#%lld\n", file_path + LEN_OF_PATH, (long long)file_stat.st_size);
	    }
            close(file);
          }

          (void) closedir (dp);
        }
        else
          perror ("Couldn't open the directory");
      }
      else
        printf("%s is not valid\n", argv[1]);
    }
    else
      perror("Couldn't get metadata");

    close(input_file);
  }
  else
    perror("invalid input");

  return 0;
}
