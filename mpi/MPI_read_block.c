/**
 * Licensed to the Scalable Computing Software (SCS) Lab under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The SCS licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/time.h>


int main( int argc, char *argv[] )
{
  int rc;
  int rank;
  MPI_File fh, fd;
  char * buf;
  MPI_Status status;
  char *source = argv[1];
  char * distination = argv[2];
  MPI_Offset offset = atoll(argv[3]);
  long count = atol(argv[4]);
  int redundent = 2048;//assume no words will execess this len
  MPI_Offset size = 0;
  MPI_Init( &argc, &argv );
  MPI_Comm_rank( MPI_COMM_WORLD, &rank );

  printf("### source %s distination %s offset %lld count %ld\n", source, distination, offset, count);

  struct timeval end, start;
  gettimeofday(&start, NULL);

  //    rc = MPI_File_open( MPI_COMM_WORLD, source, MPI_MODE_RDWR | MPI_MODE_DELETE_ON_CLOSE | MPI_MODE_CREATE, MPI_INFO_NULL, &fh );
  rc = MPI_File_open( MPI_COMM_WORLD, source, MPI_MODE_RDONLY, MPI_INFO_NULL, &fh );
  if (rc) {
    printf( "Unable to open file %s\n", source );fflush(stdout);
  }

  int sync = 8;
  if(offset == 0) sync = 0;

  MPI_File_get_size(fh, &size);
  //    if( size <= count+offset+sync +redundent ){
  //        count += sync; //it is the last block/split, so adjust the count again. recall we will read sync-marker more bytes.
  //    }else{
  //	count += sync + redundent;
  //    }

  buf = (char *) malloc( count * sizeof(char));

  //  to handle read size > 2GB per read.

  int times = 1;
  long total_size = count;
  int remain = 0;
  if(total_size > 2147483648){
    times = total_size/2147483648;
    if(times >= 1) remain = (int)(total_size%2147483648);
    if(remain != 0) times = times + 1;
  }

  int times_write = times;
  if(times == 1)
    MPI_File_read_at(fh, offset, buf, (int)count, MPI_CHAR, &status );
  else{
    while(times > 1){
      MPI_File_read_at(fh, offset, buf, 2147483648, MPI_CHAR, &status );
      offset += 2147483648;
      times --;
    }
    MPI_File_read_at(fh, offset, buf, remain, MPI_CHAR, &status );
  }

  offset = 0;

  MPI_File_close( &fh );

  rc = MPI_File_open( MPI_COMM_WORLD, distination, MPI_MODE_RDWR | MPI_MODE_CREATE, MPI_INFO_NULL, &fd );

  if(times_write == 1)
    MPI_File_write(fd, buf, (int)count, MPI_CHAR, &status);
  else{
    while(times_write > 1){
      MPI_File_write_at(fd, offset, buf, 2147483648, MPI_CHAR, &status);
      offset += 2147483648;
      times_write --;
    }
    MPI_File_write_at(fd, offset, buf, remain, MPI_CHAR, &status);
  }

  MPI_File_close( &fd);
  free(buf);
  //    printf("source %s distination %s offset %lld count %ld\n", source, distination, offset, count);

  char *SYNC = "SYNC";
  char *flag_file = malloc(strlen(distination)+strlen(SYNC)+1);//+1 for the zero-terminator
  strcpy(flag_file, distination);
  strcat(flag_file, SYNC);

  MPI_File fg;
  printf("sync file name %s\n", flag_file);

  rc = MPI_File_open( MPI_COMM_WORLD, flag_file, MPI_MODE_RDWR | MPI_MODE_CREATE, MPI_INFO_NULL, &fg );
  MPI_File_close( &fg);

  MPI_Finalize();

  gettimeofday(&end, NULL);

  /*char *mark = malloc(strlen(distination)+2);*/
  /*strcpy(mark, distination);*/
  /*strcat(mark, "R");*/

  /*FILE *f = fopen(mark, "w");*/
  /*if (f == NULL)*/
  /*{*/
    /*printf("Error opening file!\n");*/
    /*exit(1);*/
  /*}*/

  /*fprintf(f, "count %d offset %lld sync %d\n", count, offset, sync);*/
  /*fprintf(f, "%s start %lu end %lu \n", distination, 1000000*start.tv_sec+start.tv_usec, 1000000*end.tv_sec+end.tv_usec);*/
  /*fprintf(f, "Time used in reading from PFS: %lf seconds\n",*/
      /*((end.tv_sec - start.tv_sec) * 1000000 + (end.tv_usec - start.tv_usec)) / 1000000.0);*/
  /*fclose(f);*/

  return 0;
}
