# README #

A PortHadoop Implementation on Cloudera Hadoop (CDH5).

## Deployment ##

### Environment Setup ###

```shell
export HADOOP_HOME=/path/to/porthadoop/repo/hadoop-dist/target/hadoop-2.5.0-cdh5.3.3
```
* Setup other necessary environment variables, such as ```JAVA_HOME```, ```PATH```, etc.
* Install Parallel File System (PFS) clients at all Hadoop nodes.

### Dependency ###

1. Protobuf: (version 2.5) https://developers.google.com/protocol-buffers/
2. MPICH https://www.mpich.org/
    * Install MPICH at Hadoop cluster
    * Enable ROMIO PFS ADIO (optional, for performance concern)
```shell
configure --enable-romio --with-file-system=your_pfs+ufs --prefix=/path/to/install
make
make install
```

### Compile ###

* Install Apache Maven 3.0+
* Backup the configuration file
* Compile: `mvn package -Pdist,native -DskipTests -Dmaven.javadoc.skip=true`
* Update after compile and install:
```shell
cp back_up_conf_etc/hadoop/* $HADOOP_HOME/etc/hadoop/
```

### Configuration in CDH ###

#### PortHadoop ####

Firstly, Hadoop needs to be set up with proper configurations and required options for PortHadoop. Under `$HADOOP_HOME/etc/hadoop/`

* core-site.xml (PortHadoop configurations)
    ```xml
    <property>
    	<name>fs.default.name</name>
    	<value>hdfs://master_node:9005</value>
    	<final>true</final>
    </property>
    <property>
    	<name>hadoop.tmp.dir</name>
    	<value>/path/to/local/storage/dir</value>
    	<description>Local dir of hosting node for the HDFS storage.</description>
    </property>
    <property>
    	<name>pfs_path</name>
    	<value>/path/to/PFS/mount/point/</value>
    	<description>The path to PFS client. Keep the last '/'.</description>
    </property>
    <property>
    	<name>mpirun_path</name>
    	<value>/path/to/mpich_path/bin/</value>
    	<description>The path of mpirun. Keep the last '/'.</description>
    </property>
    <property>
    	<name>MPI_exec_path</name>
    	<value>/path/to/porthadoop/repo/mpi/</value>
    	<description>The path of all MPI apps. Keep the last '/'.</description>
    </property>
    <property>
    	<name>pfs_prefix</name>
    	<value>your_pfs://</value>
    	<description>To specify the target PFS.</description>
    </property>
    <property>
    	<name>viz_job</name>
    	<value>0</value>
    	<description>If viz job, set split length to a small value to avoid repetitive map.</description>
    </property>
    <property>
      <name>ram_path</name>
      <value>/dev/shm/</value>
      <description>The path to ramdisk. Keep the last '/'.</description>
    </property>
    ```

* hdfs-site.xml
    ```xml
    <property>
    	<name>dfs.replication</name>
    	<value>1</value>
    </property>
    <property>
    	<name>dfs.datanode.address</name>
    	<value>0.0.0.0:9010</value>
    </property>
    <property>
    	<name>dfs.datanode.ipc.address</name>
    	<value>0.0.0.0:9020</value>
    </property>
    <property>
    	<name>dfs.datanode.http.address</name>
    	<value>0.0.0.0:9075</value>
    </property>
    <property>
    	<name>dfs.datanode.https.address</name>
    	<value>0.0.0.0:9475</value>
    </property>
    ```

* mapred-site.xml
* yarn-site.xml
* yarn-env.sh
* slaves

Please find more detailed guidelines in those configuration files.

#### MPI ####

Next, some preparation needs to be done for the MPI file reader. Set `parthvar` in `mpi/premake.sh` to the `pfs_path` in `core-site.xml`.

```shell
pathvar="/path/to/pfs/mount/point"
```

Then,

```shell
./premake.sh
make clean
make
```

### Reserved Strings for Input Data Paths ###

PortHadoop transparently supports both native Hadoop applications and iterative applications. Therefore, PortHadoop system reserves words for identifying such hints during application lifetime.

* Word 'phr' is the reserved word by the system for identifying the PortHadoop jobs.
* Word '_out' is the hint for iterative applications.

Besides, the prefix of the parallel file system (e.g., gpfs://) identifies the PortHadoop jobs and the input data paths, such as the below example.

## Test Case ##

### Visualization and Data Analysis of WRF data in R ###

The test code for visualization and data analysis of NASA WRF data is in `vis_code/rporthadoop_map_red.R`. Before running it, modify the code to include the correct `HADOOP_HOME` and `HADOOP_STREAMING` variables.

```R
Sys.setenv(HADOOP_CMD="/path/to/your/porthadoop/repo/hadoop-dist/target/hadoop-2.5.0-cdh5.3.3/bin/hadoop")
Sys.setenv(HADOOP_STREAMING="/path/to/your/porthadoop/repo/hadoop-dist/target/hadoop-2.5.0-cdh5.3.3/share/hadoop/tools/lib/hadoop-streaming-2.5.0-cdh5.3.3.jar")
```

And modify the number of input files:

```R
nfiles <- num_of_input_files
```

At the bottom of the file, set the PFS prefix as `pfs_prefix` in `core-site.xml`:

```R1
input_path <- paste0("your_pfs_prefix://", nfiles)
```

Run it with `R --no-save -e "source('rporthadoop_map_red.R')"`

The result will be stored in `/dev/shm/reduce/` on the reducer node.


## Development ##

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Contact me ###

* Kun Feng
* fkengun@gmail.com
